# Web Service using Flask

## Endpoints

The following endpoints are available:

- `/min`: Given a list of numbers and a quantifier (how many), provides the minimum number(s).
- `/max`: Given a list of numbers and a quantifier (how many), provides the maximum number(s).
- `/avg`: Given a list of numbers, calculates their average.
- `/median`: Given a list of numbers, calculates their median.
- `/percentile`: Given a list of numbers and a quantifier 'q', computes the qth percentile of the list elements.

## Running the application

1. Clone the repository.

```
cd existing_repo
git remote add origin https://gitlab.com/anirudhcenationuniverse/math-api.git
git branch -M main
git push -uf origin main
```

2. Install the required packages by running `pip install -r requirements.txt`.
3. Run the application by running `python main.py`.

## Usage

All endpoints accept a list of numbers and a quantifier (how many) as input parameters.

For the `/min` and `/max` endpoints, the quantifier specifies how many minimum or maximum numbers to return.

For the `/percentile` endpoint, the quantifier `q` specifies which percentile to return.

## Example

The following is an example request to the `/min` endpoint:

GET /min?numbers=1,2,3,4,5&how_many=3

## Tests
To run the unit tests, execute the following command in the root directory of the project:

python -m unittest tests/test_app.py