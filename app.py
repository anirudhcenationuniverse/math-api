from flask import Flask, request
import numpy as np

app = Flask(__name__)

@app.route("/min", methods=["GET"])
def get_min():
    '''
    Given a list of numbers and a quantifier (how many), returns the minimum number(s).

    Input: A list of numbers, an integer quantifier 'k' that specifies the number of minimum values to return.
    Output: A list of the 'k' smallest numbers in the input list.
    '''
    numbers_string = request.args.get("numbers")
    numbers = [int(x) for x in numbers_string.split(',')]

    how_many = int(request.args.get("how_many"))

    minimum_numbers = sorted(numbers)[:how_many]
    return {"min": minimum_numbers}

@app.route("/max", methods=["GET"])
def get_max():
    '''
    Given a list of numbers and a quantifier (how many), returns the maximum number(s).

    Input: A list of numbers, an integer quantifier 'k' that specifies the number of maximum values to return.
    Output: A list of the 'k' largest numbers in the input list.
    '''
    numbers_string = request.args.get("numbers")
    numbers = [int(x) for x in numbers_string.split(',')]
    how_many = int(request.args.get("how_many"))

    maximum_numbers = sorted(numbers, reverse=True)[:how_many]
    return {"max": maximum_numbers}

@app.route("/avg", methods=["GET"])
def get_avg():
    '''
    Given a list of numbers, calculates their average.

    Input: A list of numbers.
    Output: The average of the numbers in the input list.
    '''
    numbers_string = request.args.get("numbers")
    numbers = [int(x) for x in numbers_string.split(',')]
    avg = np.mean(numbers)
    return {"avg": avg}

@app.route("/median", methods=["GET"])
def get_median():
    '''
    Given a list of numbers, calculates their median.

    Input: A list of numbers.
    Output: The median of the numbers in the input list.
    '''
    numbers_string = request.args.get("numbers")
    numbers = [int(x) for x in numbers_string.split(',')]
    median = np.median(numbers)
    return {"median": median}

@app.route("/percentile", methods=["GET"])
def get_percentile():
    '''
    Given a list of numbers and quantifier 'q', computes the qth percentile of the list elements.

    Input: A list of numbers, an integer quantifier 'q' that specifies the desired percentile.
    Output: The qth percentile of the numbers in the input list, computed using the nearest-rank method.
    '''
    numbers_string = request.args.get("numbers")
    numbers = [int(x) for x in numbers_string.split(',')]
    q = int(request.args.get("q"))
    percentile = np.percentile(numbers, q)
    return {"percentile": percentile}