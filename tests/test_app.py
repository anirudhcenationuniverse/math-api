import unittest
from app import app

class MinTestCase(unittest.TestCase):
    def test_min_one(self):
        with app.test_client() as c:
            r = c.get('/min?numbers=1,2,3,4,5&how_many=1')
            self.assertEqual(r.data, b'{"min":[1]}\n')

    def test_min_two(self):
        with app.test_client() as c:
            r = c.get('/min?numbers=1,2,3,4,5&how_many=2')
            self.assertEqual(r.data, b'{"min":[1,2]}\n')

class MaxTestCase(unittest.TestCase):
    def test_max_one(self):
        with app.test_client() as c:
            r = c.get('/max?numbers=1,2,3,4,5&how_many=1')
            self.assertEqual(r.data, b'{"max":[5]}\n')

    def test_max_two(self):
        with app.test_client() as c:
            r = c.get('/max?numbers=1,2,3,4,5&how_many=2')
            self.assertEqual(r.data, b'{"max":[5,4]}\n')

class AvgTestCase(unittest.TestCase):
    def test_avg(self):
        with app.test_client() as c:
            r = c.get('/avg?numbers=1,2,3,4,5')
            self.assertEqual(r.data, b'{"avg":3.0}\n')

class MedianTestCase(unittest.TestCase):
    def test_median(self):
        with app.test_client() as c:
            r = c.get('/median?numbers=1,2,3,4,5')
            self.assertEqual(r.data, b'{"median":3.0}\n')

class PercentileTestCase(unittest.TestCase):
    def test_percentile_median(self):
        with app.test_client() as c:
            r = c.get('/percentile?numbers=1,2,3,4,5&q=50')
            self.assertEqual(r.data, b'{"percentile":3.0}\n')

    def test_percentile_75(self):
        with app.test_client() as c:
            r = c.get('/percentile?numbers=1,2,3,4,5&q=75')
            self.assertEqual(r.data, b'{"percentile":4.0}\n')

if __name__ == '__main__':
    unittest.main()
